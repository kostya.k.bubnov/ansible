# Ansible роль установки веб сервера nginx+веб сервер отдает html страницу

## Description
Устанавливает nginx на ОС Ubuntu, веб сервер отдает заранее созданую страницу.
## Installation
Для робаты нужно установить ansible командой: sudo apt install ansible
и скачать файл testtask в папку /home
## Usage
Файл playbook.yml запускается командой: ansible-playbook ~/testtask/playbook.yml -i "127.0.0.2," -c local.
Он создает файл index.html в директории /var/www/html folder и копирует в него содержимое из site.html название и содержимое сайта записано  playbook.yml как page_title и page_description.
Сайт будет по ip  адресу который можно проверить введя команду: ip addr.